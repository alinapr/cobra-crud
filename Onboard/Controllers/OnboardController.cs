﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Onboard.Models;
using Onboard.ViewModel;

namespace Onboard.Controllers
{
    public class OnboardController : Controller
    {
        // GET: Onboard                         
        public ActionResult Customer()
        {
            return View();
        }
        

        public JsonResult GetCustomers()
        {
            try
            {
                using (CobraOnboardEntities db = new CobraOnboardEntities())
                {
                    var customersList = db.People.Select(x => new
                    {
                        Id = x.PersonId,
                        Name = x.Name,
                        Address1 = x.Address1,
                        Address2 = x.Address2,
                        City = x.City

                    }).ToList();

                    return Json(customersList, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(false);
            }
        }

        [HttpPost]
        public JsonResult AddCustomer(PersonViewModel customer)
        {
            try
            {
                using (CobraOnboardEntities db = new CobraOnboardEntities())
                {
                    var newCustomer = new Person
                    {
                        Name = customer.Name,
                        Address1 = customer.Address1,
                        Address2 = customer.Address2,
                        City = customer.City,
                    };

                    db.People.Add(newCustomer);
                    db.SaveChanges();
                    db.Entry(newCustomer).GetDatabaseValues();
   
                    return Json(newCustomer.PersonId, JsonRequestBehavior.AllowGet);
                        
                }
            }
            catch (Exception)
            {
                return Json(false);
            }
           
        }

        [HttpPost]
        public JsonResult UpdateCustomer(PersonViewModel customer)
        {
            try
            {
                using (CobraOnboardEntities db = new CobraOnboardEntities())
                {
                    if (db.People.Find(customer.Id) != null)
                    {
                        db.Entry(db.People.Find(customer.Id)).CurrentValues.SetValues(customer);
                        db.SaveChanges();
                        return Json(customer.Id, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(false);
                    } 
                }
            }
            catch (Exception)
            {
                return Json(false);
            }
        }


        public JsonResult DeleteCustomer(int Id)
        {
            try
            {
                using (CobraOnboardEntities db = new CobraOnboardEntities())
                {
                    if (db.OrderHeaders.Where(x => x.PersonId == Id).FirstOrDefault() == null)
                    {
                            var deletedCustomer = db.People.Where(x => x.PersonId == Id).FirstOrDefault();
                            db.People.Remove(deletedCustomer);
                            db.SaveChanges();
                            return Json(Id, JsonRequestBehavior.AllowGet);
                    }

                    else {
                            return Json(true);
                         }

                }
            }
            catch (Exception)
            {
                return Json(false);
            }
        }

                                          
        public ActionResult Orders()
        {
            return View();
        }

        public JsonResult GetOrders()
        {
            CobraOnboardEntities db = new CobraOnboardEntities();
            
            var OrdersList = db.OrderHeaders.Select(x => new
            {//customer = db.People.fin
                OrderId = x.OrderId,
                OrderDate = x.OrderDate,
                CustomerName = db.People.Where(y => y.PersonId == x.PersonId).FirstOrDefault().Name,
                ProductName = db.Products.Where(z => z.ProductId == db.OrderDetails.Where(y => y.OrderId == x.OrderId).FirstOrDefault().ProductId).FirstOrDefault().ProductName,
                Price = db.Products.Where(z => z.ProductId == db.OrderDetails.Where(y => y.OrderId == x.OrderId).FirstOrDefault().ProductId).FirstOrDefault().Price
            }).ToList();
            return Json(OrdersList, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetProducts()
        {
            using (CobraOnboardEntities db = new CobraOnboardEntities())
            {
                var productsList = db.Products.Select(x => new
                {
                    Id = x.ProductId,
                    ProductName = x.ProductName,
                    Price = x.Price

                }).ToList();

                return Json(productsList, JsonRequestBehavior.AllowGet);
                
            };

        }

        [HttpPost]
        public JsonResult AddOrder(OrderViewModel addedOrder)
        {
            try
            {
                using (CobraOnboardEntities db = new CobraOnboardEntities())
                {
                    var newOrderHeader = new OrderHeader
                    {
                        PersonId = addedOrder.CustomerId,
                        OrderDate = addedOrder.OrderDate,  
                        
                    };

                    db.OrderHeaders.Add(newOrderHeader);
                    db.SaveChanges();
                    db.Entry(newOrderHeader).GetDatabaseValues();
                    
                    var newOrderDetails = new OrderDetail
                    {
                        ProductId = addedOrder.ProductId,
                        OrderId = newOrderHeader.OrderId,
                        
                    };
                    
                    db.OrderDetails.Add(newOrderDetails);
                    db.SaveChanges();

                    return Json(newOrderHeader.OrderId, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(false);
            }
        }

        [HttpPost]
        public JsonResult EditOrder(OrderViewModel editedOrder)      
        {
            try
            {
                using (CobraOnboardEntities db = new CobraOnboardEntities())
                {
                    db.OrderHeaders.Find(editedOrder.OrderId).OrderDate = editedOrder.OrderDate;
                    db.OrderHeaders.Find(editedOrder.OrderId).PersonId = editedOrder.CustomerId;
                    db.OrderDetails.Where(x => x.OrderId == editedOrder.OrderId).FirstOrDefault().ProductId = editedOrder.ProductId;
                    db.SaveChanges();
                    return Json(editedOrder.OrderId, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(false);
            }
        }


        public JsonResult DeleteOrder(int Id)
        {
            try
            {
                using (CobraOnboardEntities db = new CobraOnboardEntities())
                {
                    db.OrderDetails.Remove(db.OrderDetails.Where(x => x.OrderId == Id).FirstOrDefault());
                    db.OrderHeaders.Remove(db.OrderHeaders.Where(x => x.OrderId == Id).FirstOrDefault());
                    db.SaveChanges();

                    return Json(Id, JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception)
            {
                return Json(false);
            }
        }

    }
}