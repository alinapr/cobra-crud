﻿onboardapp.service('onboardService', function ( $http) {

                                              
    var baseUrl = '/Onboard/';

    var urlGet = '';
    this.getAll = function (apiRoute) {
        urlGet = apiRoute;
        return $http.get(urlGet);
    }
   

    this.getCustomers = function (scope) {
        var apiRoute = baseUrl + 'GetCustomers/';
        var allCustomers = this.getAll(apiRoute);
        allCustomers.then(function (response) {
            scope.customers = response.data;
            if (response.data.length === 0)

                return response;
        });
    }

    this.getProducts = function (scope) {
        var apiRoute = baseUrl + 'GetProducts/';
        var allProducts = this.getAll(apiRoute);
        allProducts.then(function (response) {
            scope.products = response.data;

            return response;

        });
    }

    this.getOrders = function (scope) {
        var apiRoute = baseUrl + 'GetOrders/';
        var allOrders = this.getAll(apiRoute);
        allOrders.then(function (response) {
            scope.orders = response.data;
            return response;

        });
    }

 
    this.AddNewCustomer = function (person) {
        return $http({
            method: "post",
            url: "/Onboard/AddCustomer/",
            data: JSON.stringify(person),
            dataType: "json"
        });
    }

    this.UpdateCustomer = function (person) {       
        return $http({
            method: "post",
            url: "/Onboard/UpdateCustomer/",
            data: JSON.stringify(person),
            dataType: "json"
        });
    }

  
    this.deleteCustomer = function (Id) {
        return $http.post('/Onboard/DeleteCustomer/' + Id);
    }

 
    this.AddNewOrder = function (header) {
        return $http({
            method: "post",
            url: "/Onboard/AddOrder/",
            data: JSON.stringify(header),
            dataType: "json"
        });
    }


    this.EditOrder = function (header) {
        return $http({
            method: "post",
            url: "/Onboard/EditOrder/",
            data: JSON.stringify(header),
            dataType: "json"
        });
    }

    this.deleteOrder = function (OrdId) {
        return $http.post('/Onboard/DeleteOrder/' + OrdId);
    }
});