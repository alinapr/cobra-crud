﻿onboardapp.directive('formattedDatederactive', function (dateFilter) {
    return {

        require: 'ngModel',
        scope: {
            format: "="
        },

        link: function (scope, element, attrs, ngModelController) {
            ngModelController.$parsers.push(function (data) {  
                return dateFilter(data, scope.format); 
            });

            ngModelController.$formatters.push(function (data) {
                return dateFilter(data, scope.format); 
            });
        }
    }
});