﻿onboardapp.controller('modalCustomerController', function ($scope, $uibModalInstance,resultOption,addCustomerData, edit, add,deleteCustShow, customerData, selectedDataIndex, editCustomerData) {

    
    $scope.edit = edit;
    $scope.add = add;
    $scope.deleteCustShow = deleteCustShow;
    $scope.resultOption = resultOption;
    $scope.customers = customerData;
    $scope.selectedDataIndex = selectedDataIndex;
    $scope.editCustomerData = editCustomerData;
    $scope.addCustomerData = addCustomerData;

    $scope.addOrdCustName;
    $scope.addOrdProName;


    $scope.priceUpdate = function () {
        angular.forEach($scope.products, function (value, key) {
            if ($scope.modalSelectedData.ProductName === value.ProductName) {
                $scope.editOrdProPrice = value.Price;
            }
           
        });
    }


    $scope.addModalpriceUpdate = function () {
        angular.forEach($scope.products, function (value, key) {
            if ($scope.addOrdProName === value.ProductName) {
                $scope.addOrdProPrice = value.Price;
            }

        });
    }

    if (edit) {
        $scope.selCusEditModalData = angular.copy($scope.customers[selectedDataIndex]);
    }
    

    $scope.saveEdit = function () {
        $scope.resultOption = 'editCustomer';
        $scope.editCustomerData = { Id: $scope.selCusEditModalData.Id, name: $scope.selCusEditModalData.Name, address1: $scope.selCusEditModalData.Address1, address2: $scope.selCusEditModalData.Address2, city: $scope.selCusEditModalData.City }

        $uibModalInstance.close({ data: $scope.editCustomerData, option: $scope.resultOption });

    };

    $scope.saveAdd = function () {
        $scope.resultOption = 'addCustomer';
        $scope.addCustomerData = { name: $scope.customerName, address1: $scope.customerAddress1, address2: $scope.customerAddress2, city: $scope.customerCity }
        $uibModalInstance.close({ data: $scope.addCustomerData, option: $scope.resultOption });

    };

    $scope.delCustomer = function () {
        $scope.resultOption = 'deleteCust';
        $uibModalInstance.close({ option: $scope.resultOption });
    };


    $scope.ok = function () {

        $uibModalInstance.close("ok");
    };

    $scope.cancel = function ($value) {
        $scope.resultOption = '';
        
        $uibModalInstance.dismiss("cancel");

    };
});