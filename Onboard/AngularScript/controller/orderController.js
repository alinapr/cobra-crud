﻿

onboardapp.controller('orderController', function ($scope, onboardService, dateFilter, $uibModal, $log, $timeout) {

    $scope.square = function () {
        $scope.result = onboardService.square($scope.number);
    }
 
    var baseUrl = '/Onboard/';
    $scope.customers = [];
    $scope.orders = [];
    $scope.products = [];
    onboardService.getCustomers($scope);
    onboardService.getProducts($scope);
    onboardService.getOrders($scope);
    

    $scope.editCustomerData = { Id: "", name: "", address1: "", address2: "", city: "" }
    $scope.addCustomerData = { name: "", address1: "", address2: "", city: "" }
    $scope.selectedDataIndex;
    $scope.addOrderShow = false;
    $scope.editOrderShow = false;
    $scope.deleteOrderShow = false;
    $scope.resultOption = '';

   

    $scope.open = function (index, option) {
        $scope.selectedDataIndex = index;

        switch (option) {
           
            case 'editOrderShow':
                $scope.addOrderShow = false;
                $scope.editOrderShow = true;
                $scope.deleteOrderShow = false;
                break;
            case 'addOrderShow':
                $scope.addOrderShow = true;
                $scope.editOrderShow = false;
                $scope.selectedDataIndex = 0; 
                $scope.deleteOrderShow = false;
                break;
            case 'deleteOrder':
                $scope.deleteOrderShow = true;
                $scope.addOrderShow = false;
                $scope.editOrderShow = false;

            default:
        }
        

        var modalInstance = $uibModal.open({
            templateUrl: '/Templates/orderModal.html',
            controller: 'modalOrderController',
            resolve: {
                resultOption: function () {
                    return $scope.resultOption;
                },
                editOrderShow: function () {
                    return $scope.editOrderShow;
                },
                addOrderShow: function () {
                    return $scope.addOrderShow;
                },
                deleteOrderShow: function(){
                    return $scope.deleteOrderShow;
                },
                orders: function(){
                    return $scope.orders;
                },
                selectedDataIndex: function () {
                    return $scope.selectedDataIndex;
                },
                customerData: function () {
                    return $scope.customers;
                },
                products: function () {
                    return $scope.products;
        }

            }
        });

        modalInstance.result.then(function (transferData) {
           
            switch (transferData.option) {               
                case 'addOrder':
                    var orderService = onboardService.AddNewOrder(transferData.orderHeader);

                    orderService.then(function (response) {
                        if(response.data > 0){
                            $scope.successMessage = "Order has been successfully saved.";                        
                            $('#order-dialog').modal('toggle');

                            $scope.retOrderId = response.data;
                            var date = new Date(transferData.orderHeader.OrderDate);
                            var jsonDate = '/Date(' + date.getTime() + ')/'; 
                            $scope.newOrder = {
                            OrderId: $scope.retOrderId,
                            OrderDate: jsonDate,
                            CustomerName: transferData.addOrderDet.CustomerName,
                            ProductName: transferData.addOrderDet.ProductName,
                            Price: transferData.addOrderDet.Price
                        }

                        $scope.orders.push($scope.newOrder);
                        } else {
                            $scope.successMessage = "Fail!";
                            $('#order-dialog').modal('toggle');
                        }
                    })
                    break;
                case 'editOrder':
                    var orderService = onboardService.EditOrder(transferData.orderHeader);
                    orderService.then(function (response) {
                        if (response.data > 0) {
                            $scope.successMessage = "Order has been saved.";
                            $('#order-dialog').modal('toggle');

                            $scope.retOrderId = response.data;
                            $scope.editedOrderObj = $scope.orders[index]; 
                            if ($scope.retOrderId == $scope.editedOrderObj.OrderId) {
                                var date = new Date(transferData.orderHeader.OrderDate);
                                var jsonDate = '/Date(' + date.getTime() + ')/'; 
                                $scope.editedOrderObj.OrderDate = jsonDate;
                                $scope.editedOrderObj.CustomerName = transferData.editDet.CustomerName;
                                $scope.editedOrderObj.ProductName = transferData.editDet.ProductName;
                                $scope.editedOrderObj.Price = transferData.editDet.Price;
                            } else {
                                $scope.successMessage = "Order ID is not matching.";
                                $('#success-modal').modal('toggle');
                            }
                        } else {
                            $scope.successMessage = "Edit failed.";
                            $('#order-dialog').modal('toggle');
                        }
                    })
                    break;
                case 'delOrder':
                    var currentRecord = $scope.orders[index];
                    var orderId = currentRecord.OrderId;

                    var orderService = onboardService.deleteOrder(orderId);
                    orderService.then(function (response) {
                        if(response.data > 0){
                            $scope.orders.splice(index, 1);
                            $scope.successMessage = "Order has been deleted.";
                            $('#order-dialog').modal('toggle');
                        } else {
                            $scope.successMessage = "fail";
                            $('#order-dialog').modal('toggle');
                        }
                        
                    }, function (error) {
                        $scope.successMessage = "Server error";
                        $('#order-dialog').modal('toggle');
                    });
                default:
            }
            
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

});

